package com.alecu.tutorials.spring.boot.mongo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired UserRepository userRepository;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<UserModel> create(@RequestBody UserModel user) {
        userRepository.save(user);
        return new ResponseEntity<UserModel>(user, HttpStatus.OK);
    }
    
    @RequestMapping
    public ResponseEntity<List<UserModel>> list() {
        List<UserModel> users = userRepository.findAll();
        return new ResponseEntity<List<UserModel>>(users, HttpStatus.OK);
    }

}
